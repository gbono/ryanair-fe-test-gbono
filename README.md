# Ryanair Fronteend Test (Angular 5) - Giampiero Bono

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.8.

## Project setup + start

Project generated with:
`node v10.0.0`
`yarn v1.6.0`

1. `git clone git@gitlab.com:gbono/ryanair-fe-test-gbono.git`
2. `cd ryanair-fe-test-gbono`
3. `yarn install`
3. `yarn start`

The application will be built using webpack and it can be reached via browser thanks to a dev server. 

## Architectural decisions

- Project has been generated from scratch with Angular CLI `v1.6.8`. This tool allowed a dev speed up.
- UI components (date picker and type ahead) by [ng-bootstrap](https://ng-bootstrap.github.io/)
- Application UI components react to application states changes.
This pattern comes out from React team but it has been ported to Angular thanks to[ngrx library](https://github.com/ngrx/platform).
- The interaction between the UI and the store is done using specific Angular services, acting as a kind of "data access layer". 
This is done to allow a complete decouple and a better organized architecture.
- UI Components are split by container / presenter structure as recommended by the React team
- Used some pure function helpers to manipulate data and perform operations without any side effect.
- Used interfaces instead of classes to compose instead of inherit the business object model.

## Functional view

The UI interface is kept simple and the colors chosen come directly from the Ryan Air website. 

The interface is composed by:
- 2 location pickers (built using ng-bootstrap typeahed) to chose outbound and inbound airports. The airport list to filter
arrive directly from the backend and as soon as the user starts typing, the filtered result will be presented.
Considering that "not all the airports can reach other airports", the inbound location picker list is filled only after the outbound section.
Every time the user will update the outbound location, the inbound selection will be automatically reset and the location picker list will be update.
 
- 2 date pickers (built using ng-bootstrap date picker) to chose fly out and fly back dates.  
Since Ryan Air does not perform time travels, the code will ensure user won't be able to chose an inbound date that is before
outbound. 

- 1 button to perform the backend call and retrieve flights availabilities information. This button will become enable as soon as 
all the form fields are valid and filled.

## Style points

- The base I am using is Bootstrap 4.
- Some mixins created to generate resusable rules.
- Trying to follow a mobile first approach
- Designed to have a view ready for mobile, tablet and desktop. 

## Tests

- Services, ngrx reducers and effects fully tested.

## Missing points

- Test for UI components. Because of this I cannot ensure all the scenarios are working properly. 

# Main npm tasks

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
The same could be done with `ng serve --aot` to perform an AOT compilation

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
