import { Component, OnInit } from '@angular/core';
import { GlobalInfoService } from './services/global-info/global-info.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(private _globalInfoService: GlobalInfoService) {
  }

  ngOnInit() {
    this._globalInfoService.loadGlobalInfo();
  }
}
