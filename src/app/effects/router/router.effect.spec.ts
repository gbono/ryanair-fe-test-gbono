import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs/observable';
import { RouterEffects } from './router.effects';

describe('Router effect', () => {
  let metadata: EffectsMetadata<RouterEffects>;
  let effects: RouterEffects;
  let actions: Observable<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([])
      ],
      providers: [
        RouterEffects,
        provideMockActions(() => actions),
      ]
    });
    effects = TestBed.get(RouterEffects);
    metadata = getEffectsMetadata(effects);
  });

  it('should register navigate$ that does not dispatches an action', () => {
    expect(metadata.navigate$).toEqual({ dispatch: false });
  });
});
