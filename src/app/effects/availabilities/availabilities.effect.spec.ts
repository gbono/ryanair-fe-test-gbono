import { TestBed } from '@angular/core/testing';
import { Action, Store } from '@ngrx/store';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { AvailabilitiesEffect } from './availabilities.effect';
import { Observable } from 'rxjs/observable';
import { Subject } from 'rxjs/Subject';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { AvailabilitiesService } from '../../services/availabilities/availabilities.service';
import { DataProviderService } from '../../services/data-provider/data-provider.service';
import { MockDataProviderService } from '../../services/data-provider/mock-data-provider.service';
import { AvailabilitiesActionTypes, LoadAvailabilites } from '../../actions/availabilities/availabilities.actions';
import { Search } from '../../models';

describe('Availabiltiy effect', () => {
  let metadata: EffectsMetadata<AvailabilitiesEffect>;
  let effects: AvailabilitiesEffect
  let dataProvider: DataProviderService
  let actions: Observable<any>;
  const subject = new Subject<any>();
  const mockStore = {
    select: jasmine.createSpy('select').and.returnValue(subject),
    dispatch: jasmine.createSpy('dispatch')
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AvailabilitiesEffect,
        AvailabilitiesService,
        provideMockActions(() => actions),
        { provide: Store, useValue: mockStore },
        { provide: DataProviderService, useClass: MockDataProviderService }
      ]
    });
    effects = TestBed.get(AvailabilitiesEffect);
    dataProvider = TestBed.get(DataProviderService);

    metadata = getEffectsMetadata(effects);
  });

  it('should register load$ that dispatches an action', () => {
    expect(metadata.load$).toEqual({ dispatch: true });
  });

  it('should register routerNavigation$ that dispatches an action', () => {
    expect(metadata.routerNavigation$).toEqual({ dispatch: true });
  });

  it('should do an UPDATE if succeeded', () => {
    const action = new ReplaySubject(1);
    const fakeSearch: Search = {
      outboundAirport: 'PEG',
      inboundAirport: 'CAG',
      outboundDate: new Date('2018-05-13T00:00:00.000Z'),
      inboundDate: new Date('2018-05-20T00:00:00.000Z')
    };
    action.next(new LoadAvailabilites(dataProvider.getCheapFlightsData(fakeSearch).toPromise()));

    effects.load$.subscribe((result: Action) => {
      expect(result.type).toEqual(AvailabilitiesActionTypes.Update);
    });
  });
});
