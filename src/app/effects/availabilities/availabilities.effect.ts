import { Injectable } from '@angular/core';

import { Actions as RxActions, Effect, ofType } from '@ngrx/effects';
import { ROUTER_NAVIGATION, RouterNavigationAction } from '@ngrx/router-store';
import { fromPromise } from 'rxjs/observable/fromPromise';

import { catchError, filter, map, mergeMap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import {
  AvailabilitiesActionTypes, FailAvailabilities, LoadAvailabilites, UpdateAvailabilities
} from '../../actions/availabilities/availabilities.actions';
import { Search } from '../../models/data';
import { AvailabilitiesService } from '../../services/availabilities/availabilities.service';
import { allPropsDefined, isObjEmptyOrUndefined } from '../../common/helpers/miscellaneous.helpers';
import { NewStateFromUrl } from '../../actions/search/search.actions';
import { routerParamsToSearch } from '../../common/helpers/url.helpers';

@Injectable()
export class AvailabilitiesEffect {

  @Effect()
  load$ = this.actions$.pipe(ofType(AvailabilitiesActionTypes.Load),
    mergeMap((action: LoadAvailabilites) => fromPromise(action.asyncPayload).pipe(
      map((reply) => new UpdateAvailabilities(reply)),
      catchError((err) => of(new FailAvailabilities(err)))
    )));

  @Effect()
  routerNavigation$ = this.actions$.pipe(ofType(ROUTER_NAVIGATION),
    filter((action: RouterNavigationAction) =>
      action.payload.routerState.url.indexOf('/search') > -1
      && !isObjEmptyOrUndefined(action.payload.routerState['params'])),
    map((action: RouterNavigationAction) => {
      const routerParams: Search = action.payload.routerState['params'];
      if (routerParams && allPropsDefined(routerParams)) {
        this.availabilitiesService.loadFlights(routerParams);
        return new NewStateFromUrl(routerParamsToSearch(routerParams));
      }
    }));

  constructor(protected actions$: RxActions,
              protected availabilitiesService: AvailabilitiesService) {
  }
}
