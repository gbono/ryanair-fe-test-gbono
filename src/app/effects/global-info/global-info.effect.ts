import { Injectable } from '@angular/core';

import { Actions as RxActions, Effect, ofType } from '@ngrx/effects';
import { fromPromise } from 'rxjs/observable/fromPromise';

import { catchError, map, mergeMap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';

import {
  FailGlobalInfo,
  GlobalInfoActionTypes,
  LoadGlobalInfo,
  UpdateGlobalInfo
} from '../../actions/global-info/global-info.actions';

@Injectable()
export class GlobalInfoEffect {

  @Effect()
  load$ = this.actions$.pipe(ofType(GlobalInfoActionTypes.Load),
    mergeMap((action: LoadGlobalInfo) => fromPromise(action.asyncPayload).pipe(
      map((reply) => new UpdateGlobalInfo(reply)),
      catchError((err) => of(new FailGlobalInfo(err)))
    )));

  constructor(protected actions$: RxActions) {
  }

}
