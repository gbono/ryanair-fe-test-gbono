import { TestBed } from '@angular/core/testing';
import { Action } from '@ngrx/store';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs/observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { DataProviderService } from '../../services/data-provider/data-provider.service';
import { GlobalInfoEffect } from './global-info.effect';
import { MockDataProviderService } from '../../services/data-provider/mock-data-provider.service';
import { GlobalInfoActionTypes, LoadGlobalInfo } from '../../actions/global-info/global-info.actions';

describe('GlobalInfo effect', () => {
  let metadata: EffectsMetadata<GlobalInfoEffect>;
  let effects: GlobalInfoEffect;
  let actions: Observable<any>;
  let dataProvider: DataProviderService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        GlobalInfoEffect,
        provideMockActions(() => actions),
        { provide: DataProviderService, useClass: MockDataProviderService }
      ]
    });
    effects = TestBed.get(GlobalInfoEffect);
    dataProvider = TestBed.get(DataProviderService);
    metadata = getEffectsMetadata(effects);
  });

  it('should register load$ that dispatches an action', () => {
    expect(metadata.load$).toEqual({ dispatch: true });
  });

  it('should do an UPDATE if succeeded', () => {
    const action = new ReplaySubject(1);
    action.next(new LoadGlobalInfo(dataProvider.getGlobalInfoData().toPromise()));

    effects.load$.subscribe((result: Action) => {
      expect(result.type).toEqual(GlobalInfoActionTypes.Update);
    });
  });
});
