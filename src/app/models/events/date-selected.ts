import { BoundType } from '../enum';

export interface DateSelected {
  boundType: BoundType;
  selectedDate: Date;
}
