export enum BoundType {
  Inbound = 'inbound',
  Outbound = 'outbound'
}
