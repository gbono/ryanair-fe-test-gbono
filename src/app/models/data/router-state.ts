import { NavigationExtras } from "@angular/router";

export interface RouterState {
  path: any[];
  query?: object;
  extras?: NavigationExtras;
}
