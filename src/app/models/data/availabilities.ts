import { Flight } from './flight';

export interface Availabilities {
  flights: Flight[];
}
