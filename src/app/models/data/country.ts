export interface Country {
    code: string;
    name: string;
    seoName: string;
    englishSeoName: string;
    currency: string;
    url: string;
}
