export interface DiscountType {
    globalCode: string;
    code: string;
    name: string;
    percentage: number;
  }
