export interface Flight {
  dateFrom: string;
  dateTo: string;
  currency: string;
  price: number;
}
