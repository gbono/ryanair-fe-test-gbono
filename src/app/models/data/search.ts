export interface Search {
    outboundDate: Date | null;
    inboundDate: Date | null;
    outboundAirport: string | null;
    inboundAirport: string | null;
  }
