import { Country } from './country';

export interface Airport {
    iataCode: string;
    name: string;
    base: boolean;
    latitude: number;
    longitude: number;
    country: Country;
}
