import { Airport } from './airport';
import { Country } from './country';
import { DiscountType } from './discount-type';

export interface GlobalInfo {
  routes: { [code: string]: string[]; } | {};
  airports: Airport[];
  discounts: {
    routes: { [code: string]: string[]; };
    countries: { [code: string]: string[]; };
    types: DiscountType[];
  } | {};
  countries: Country[];
  messages: { [type: string]: string; } | {};
}
