import { Action } from '../../common/action/action';
import { Airport, Search } from '../../models';
import { BaseAction } from '../../common/action/base-action';

export enum SearchActionType {
  SelectOutboundAirport = '[Search] Select outbound location',
  SelectOutboundDate = '[Search] Select outbound date',
  ResetOutboundDate = '[Search] Reset outbound date',
  SelectInboundAirport = '[Search] Select inbound location',
  SelectInboundDate = '[Search] Select inbound date',
  ResetInboundDate = '[Search] Reset inbound date',
  ResetSearchData = '[Search] Reset all search data',
  ResetInboundAirport = '[Search] Reset inbound airport',
  NewStateFromUrl = '[Search] New state search from Url'
}

export class SelectInboundAirport implements Action<Airport> {
  readonly type = SearchActionType.SelectInboundAirport;

  constructor(public payload: Airport) {
  }
}

export class SelectOutboundAirport implements Action<Airport> {
  readonly type = SearchActionType.SelectOutboundAirport;

  constructor(public payload: Airport) {
  }
}

export class SelectInboundDate implements Action<Date> {
  readonly type = SearchActionType.SelectInboundDate;

  constructor(public payload: Date) {
  }
}

export class SelectOutboundDate implements Action<Date> {
  readonly type = SearchActionType.SelectOutboundDate;

  constructor(public payload: Date) {
  }
}

export class ResetOutboundDate implements BaseAction {
  readonly type = SearchActionType.ResetOutboundDate;

  constructor() {
  }
}

export class ResetInboundDate implements BaseAction {
  readonly type = SearchActionType.ResetInboundDate;

  constructor() {
  }
}

export class ResetSearchData implements BaseAction {
  readonly type = SearchActionType.ResetSearchData;

  constructor() {
  }
}

export class ResetInboundAirport implements BaseAction {
  readonly type = SearchActionType.ResetInboundAirport;

  constructor() {
  }
}

export class NewStateFromUrl implements Action<Search> {
  readonly type = SearchActionType.NewStateFromUrl;

  constructor(public payload: Search) {
  }
}

export type LocationPickerActions = SelectInboundAirport |
  SelectOutboundAirport |
  SelectInboundDate |
  SelectOutboundDate |
  ResetOutboundDate |
  ResetInboundDate |
  ResetInboundAirport |
  ResetSearchData |
  NewStateFromUrl;
