import { Action } from '../../common/action/action';
import { RouterState } from '../../models/data/router-state';

export enum RouterActionsTypes {
  Go = '[Router] Go'
}

export class Go implements Action<Partial<RouterState>> {
  readonly type = RouterActionsTypes.Go;

  constructor(public payload: Partial<RouterState>) {
  }
}

export type RouterActionsUnion = Go;
