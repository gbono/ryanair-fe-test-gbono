import { AsyncAction } from '../../common/action/async-action';
import { GlobalInfo } from '../../models/data/global-info';
import { Action } from '../../common/action/action';
import { BaseAction } from '../../common/action/base-action';

export enum GlobalInfoActionTypes {
  Load = '[GlobalInfo] Load global information',
  Update = '[GlobalInfo] Update global information',
  Fail = '[GlobalInfo] Fail retrieving global information'
}

export class LoadGlobalInfo implements AsyncAction<GlobalInfo> {
  readonly type = GlobalInfoActionTypes.Load;

  constructor(public asyncPayload: Promise<GlobalInfo>) {
  }
}
export class UpdateGlobalInfo implements Action<GlobalInfo> {
  readonly type = GlobalInfoActionTypes.Update;

  constructor(public payload: GlobalInfo) {
  }
}
export class FailGlobalInfo implements BaseAction {
  readonly type = GlobalInfoActionTypes.Fail;

  constructor(public error: any) {
  }
}

export type GlobalInfoActions = LoadGlobalInfo | UpdateGlobalInfo | FailGlobalInfo;
