import { AsyncAction } from '../../common/action/async-action';
import { Action } from '../../common/action/action';
import { BaseAction } from '../../common/action/base-action';
import { Availabilities } from '../../models/data';

export enum AvailabilitiesActionTypes {
  Load = '[Availabilities] Load availabilities information',
  Update = '[Availabilities] Update availabilities information',
  Fail = '[Availabilities] Fail availabilities global information'
}

export class LoadAvailabilites implements AsyncAction<Availabilities> {
  readonly type = AvailabilitiesActionTypes.Load;

  constructor(public asyncPayload: Promise<Availabilities>) {
  }
}

export class UpdateAvailabilities implements Action<Availabilities> {
  readonly type = AvailabilitiesActionTypes.Update;

  constructor(public payload: Availabilities) {
  }
}

export class FailAvailabilities implements BaseAction {
  readonly type = AvailabilitiesActionTypes.Fail;

  constructor(public error: any) {
  }
}

export type AvailabilitiesActions = LoadAvailabilites | UpdateAvailabilities | FailAvailabilities;
