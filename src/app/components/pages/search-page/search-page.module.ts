import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CommonModule } from '@angular/common';
import { SearchPageComponent } from './search-page.component';
import { ROUTES } from './search.route';
import { FlightSearchModule } from '../../shared/flight-search/flight-search.module';

@NgModule({
  imports: [
    CommonModule,
    FlightSearchModule,
    RouterModule.forChild(ROUTES)
  ],
  declarations: [SearchPageComponent]
})
export class SearchPageModule {
}
