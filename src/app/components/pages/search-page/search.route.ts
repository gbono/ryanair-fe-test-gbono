import { Routes } from '@angular/router';
import { SearchPageComponent } from './search-page.component';

export const ROUTES: Routes = [{
  path: '',
  component: SearchPageComponent,
  children: [
    {
      path: 'search/:outboundAirport/:inboundAirport/:outboundDate/:inboundDate',
      loadChildren: '../../shared/availability-table/availability-table.module#AvailabilityTableModule',
    },
  ],
}];
