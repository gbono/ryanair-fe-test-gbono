import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';
import { LocationPickerComponent } from './location-picker.component';

@NgModule({
  imports: [
    CommonModule,
    NgbTypeaheadModule,
    FormsModule,
  ],
  declarations: [LocationPickerComponent],
  exports: [LocationPickerComponent],
})
export class LocationPickerModule {
}
