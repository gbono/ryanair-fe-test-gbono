import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { NgbTypeahead, NgbTypeaheadSelectItemEvent } from '@ng-bootstrap/ng-bootstrap';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/merge';
import { Observable } from 'rxjs/Observable';
import { filter } from 'rxjs/operators';
import { Subject } from 'rxjs/Subject';
import {
  airportNameContains, getAirportFromAirportName, getAirportName
} from '../../../common/helpers/location.helpers';
import { Airport } from '../../../models/data';

let nextId = 0;

@Component({
  selector: 'app-location-picker',
  templateUrl: './location-picker.component.html',
  styleUrls: ['./location-picker.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LocationPickerComponent {

  @Input()
  public airports: Airport[];

  @Input()
  public preSelectedAirport?: Airport;

  @Input()
  public textLabel: string;

  @Output()
  public selectedAirport: EventEmitter<Airport> = new EventEmitter<Airport>();

  public model: any;
  public id = `ryan-air-date-picker-${nextId++}`;

  focus$ = new Subject<string>();
  click$ = new Subject<string>();
  @ViewChild('instance') instance: NgbTypeahead;

  searchAirport = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .distinctUntilChanged()
      .merge(this.focus$)
      .merge(this.click$.pipe(filter(() => !this.instance.isPopupOpen())))
      .map(searchString => {
        const airportNameContainsSearch = airportNameContains(searchString);
        return (searchString === '' ? this.airports : this.airports.filter(airportNameContainsSearch)).map(getAirportName);
      })

  onAirportSelected(ngbTypeaheadSelectItemEvent: NgbTypeaheadSelectItemEvent): void {
    if (ngbTypeaheadSelectItemEvent && ngbTypeaheadSelectItemEvent.item) {
      this.selectedAirport.emit(getAirportFromAirportName(ngbTypeaheadSelectItemEvent.item, this.airports));
    }
  }
}

