import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { NgbCalendar, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import {
  addMonths, dateToNgbDataStruct, isNgbDataStructAfter, ngbDataStructToDate
} from '../../../common/helpers/date.helpers';
import { FormControl } from '@angular/forms';

let nextId = 0;

@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatePickerComponent {

  @Input()
  forcedMinDate?: Date;

  @Input()
  preselectedDate: Date;

  @Input()
  public formControlToAssign: FormControl;

  @Input()
  public textLabel: string;

  @Output()
  public dateSelected: EventEmitter<Date> = new EventEmitter<Date>();

  public minDate: NgbDateStruct;
  public maxDate: NgbDateStruct;

  public dateToNgbDataStruct = dateToNgbDataStruct;
  public isNgbDataStructAfter = isNgbDataStructAfter;
  public model: NgbDateStruct;
  public id = `ryan-air-date-picker-${nextId++}`;

  private readonly MAX_MONTHS_TO_ADD = 11;

  constructor(private _calendar: NgbCalendar) {
    this.minDate = this._calendar.getToday();
    const addMonthsToCurrentDate = addMonths(this.MAX_MONTHS_TO_ADD);
    const maxDateObj: Date = addMonthsToCurrentDate(ngbDataStructToDate(this.minDate));
    this.maxDate = dateToNgbDataStruct(maxDateObj);
  }

  public onDateSelected(dateSelected: NgbDateStruct) {
    this.dateSelected.emit(ngbDataStructToDate(dateSelected));
  }

  public convertDateToNgbDateStructAndEmit(newDate: Date) {
    this.dateSelected.emit(newDate);
    return dateToNgbDataStruct(newDate);
  }
}
