import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Airport, BoundType } from '../../../../../models';

@Component({
  selector: 'app-location-search-pres',
  templateUrl: './location-search-pres.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LocationSearchPresComponent {

  @Input()
  public outboundAirports: Airport[];

  @Input()
  public inboundAirports: Airport[];

  @Input()
  public preSelectedOutboundAirport: Airport;

  @Input()
  public preSelectedInboundAirport: Airport;

  @Output()
  public selectedAirport: EventEmitter<{ airport: Airport, boundType: BoundType }> =
    new EventEmitter<{ airport: Airport, boundType: BoundType }>();

  public boundTypeEnum = BoundType;

  public onAirportSelected(selectedAirport: Airport, boundType: BoundType) {
    this.selectedAirport.emit({ airport: selectedAirport, boundType: boundType });
  }
}
