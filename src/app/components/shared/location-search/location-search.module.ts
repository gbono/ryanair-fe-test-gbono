import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GlobalInfoService } from '../../../services/global-info/global-info.service';
import { SearchService } from '../../../services/search/search.service';
import { LocationPickerModule } from '../../elements/location-picker/location-picker.module';
import { LocationSearchContComponent } from './containers/location-search-cont/location-search-cont.component';
import { LocationSearchPresComponent } from './presenters/location-search-pres/location-search-pres.component';
import { DatePickerModule } from '../../elements/date-picker/date-picker.module';

@NgModule({
  imports: [
    CommonModule,
    LocationPickerModule,
    DatePickerModule
  ],
  providers: [GlobalInfoService, SearchService],
  declarations: [LocationSearchContComponent, LocationSearchPresComponent],
  exports: [LocationSearchContComponent, LocationSearchPresComponent]
})
export class LocationSearchModule {
}
