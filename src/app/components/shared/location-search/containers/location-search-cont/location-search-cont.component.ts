import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Airport, BoundType } from '../../../../../models/';
import { GlobalInfoService } from '../../../../../services/global-info/global-info.service';

import { SearchService } from '../../../../../services/search/search.service';
import { getAirportFromIataCode } from '../../../../../common/helpers/location.helpers';

@Component({
  selector: 'app-location-search-cont',
  templateUrl: './location-search-cont.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LocationSearchContComponent {

  getAirportFromIataCode = getAirportFromIataCode;

  constructor(public globalInfoService: GlobalInfoService,
              public searchService: SearchService) {
  }

  public onAirportSelected  (airportSelection: { airport: Airport, boundType: BoundType }): void {
    switch (airportSelection.boundType) {
      case BoundType.Inbound:
        this.searchService.setInboundAirport(airportSelection.airport);
        break;
      case BoundType.Outbound:
        this.searchService.setOutboundAirport(airportSelection.airport);
        this.searchService.resetInboundLocation();
        break;
      default:
        throw new Error('Unknown bound type');
    }
  }
}
