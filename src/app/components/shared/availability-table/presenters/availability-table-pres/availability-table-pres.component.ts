import { Component, Input } from '@angular/core';
import { Flight } from '../../../../../models/data';

@Component({
  selector: 'app-availability-table-pres',
  templateUrl: './availability-table-pres.component.html',
  styleUrls: ['./availability-table-pres.component.scss']
})
export class AvailabilityTablePresComponent {

  @Input()
  public flights: Flight[];

  public sortByPrice(flights: Flight[]): Flight[] {
    return [...flights].sort((x, y) => x.price - y.price);
  }
}
