import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AvailabilityTableContComponent } from './containers/availability-table-cont/availability-table-cont.component';
import { AvailabilityTablePresComponent } from './presenters/availability-table-pres/availability-table-pres.component';
import { RouterModule } from '@angular/router';
import { AvailabilitiesService } from '../../../services/availabilities/availabilities.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([{ path: '', component: AvailabilityTableContComponent }]),
  ],
  declarations: [AvailabilityTableContComponent, AvailabilityTablePresComponent],
  exports: [AvailabilityTableContComponent],
  providers: [AvailabilitiesService]
})
export class AvailabilityTableModule {
}
