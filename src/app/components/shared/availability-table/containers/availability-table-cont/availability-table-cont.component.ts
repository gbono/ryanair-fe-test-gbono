import { Component } from '@angular/core';
import { AvailabilitiesService } from '../../../../../services/availabilities/availabilities.service';

@Component({
  selector: 'app-availability-table-cont',
  templateUrl: './availability-table-cont.component.html',
})
export class AvailabilityTableContComponent {

  constructor(public availabilitiesService: AvailabilitiesService) {
  }
}
