import { Component } from '@angular/core';

import { SearchService } from '../../../../../services/search/search.service';
import { Search } from '../../../../../models/data';
import { allPropsDefined } from '../../../../../common/helpers/miscellaneous.helpers';
import { AvailabilitiesService } from '../../../../../services/availabilities/availabilities.service';
import { buildUrlFromSearch, routerParamsToSearch } from '../../../../../common/helpers/url.helpers';
import { RouterService } from '../../../../../services/router/router.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-flight-search-cont',
  templateUrl: './flight-search-cont.component.html',
  styleUrls: ['./flight-search-cont.component.scss']
})
export class FlightSearchContComponent {

  constructor(public searchService: SearchService,
              public routerService: RouterService,
              private _availabilityService: AvailabilitiesService) {
  }

  public onSubmit(): void {
    this.searchService.searchState$.pipe(take(1)).subscribe((search: Search) =>
      this.routerService.routerNavigate({ path: [buildUrlFromSearch('search')(search)] }));
  }

  public isSearchButtonEnabled(search: Search): boolean {
    return !allPropsDefined(search);
  }

  public updateSearchStateFromRouterParams(params: { [key: string]: any }): void {
    const newSearchFromUrl: Search = routerParamsToSearch(params);
    if (allPropsDefined(newSearchFromUrl)) {
      this._availabilityService.loadFlights(newSearchFromUrl);
      this.searchService.setNewStateFromUrl(newSearchFromUrl);
    }
  }
}
