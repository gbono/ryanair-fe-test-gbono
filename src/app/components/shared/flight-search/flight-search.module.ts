import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { FlightSearchContComponent } from './containers/flight-search-cont/flight-search-cont.component';
import { LocationSearchModule } from '../location-search/location-search.module';
import { DateSearchModule } from '../date-search/date-search.module';
import { SearchService } from '../../../services/search/search.service';
import { AvailabilitiesService } from '../../../services/availabilities/availabilities.service';
import { RouterService } from '../../../services/router/router.service';

@NgModule({
  imports: [
    CommonModule,
    LocationSearchModule,
    DateSearchModule,
    ReactiveFormsModule
  ],
  declarations: [FlightSearchContComponent],
  exports: [FlightSearchContComponent],
  providers: [SearchService, AvailabilitiesService, RouterService]
})
export class FlightSearchModule {
}
