import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, NgForm, Validators } from '@angular/forms';

import { BoundType, DateSelected } from '../../../../../models';
import { SearchService } from '../../../../../services/search/search.service';

@Component({
  selector: 'app-date-search-pres',
  templateUrl: './date-search-pres.component.html',
})
export class DateSearchPresComponent implements OnInit {

  @Input()
  public minInboundDate: Date;

  @Input()
  public inboundPreselectedDate: Date;

  @Input()
  public outboundPreselectedDate: Date;

  @Output()
  public onDateSelected: EventEmitter<DateSelected> = new EventEmitter<DateSelected>();

  public form: NgForm;
  public boundTypeEnum = BoundType;

  inboundDatePicker: FormControl;
  outboundDatePicker: FormControl;

  constructor(private _formBuilder: FormBuilder,
              private _searchService: SearchService) {
  }

  ngOnInit() {
    this.outboundDatePicker = new FormControl('', Validators.required);
    this.inboundDatePicker = new FormControl('', Validators.required);
  }

  public dateSelected(dateSelected: DateSelected): void {
    this.onDateSelected.emit(dateSelected);
  }
}
