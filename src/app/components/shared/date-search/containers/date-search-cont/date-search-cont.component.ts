import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { NgForm } from '@angular/forms';

import { BoundType, DateSelected } from '../../../../../models';
import { SearchService } from '../../../../../services/search/search.service';

@Component({
selector: 'app-date-search-cont',
  templateUrl: './date-search-cont.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DateSearchContComponent {

  @Input()
  public form: NgForm;

  public minInboundDate: Date;

  constructor(public searchService: SearchService) {
    this.minInboundDate = new Date();
  }

  public onDateSelected(dateSelected: DateSelected) {
    switch (dateSelected.boundType) {
      case BoundType.Inbound:
        this.searchService.setInboundDate(dateSelected.selectedDate);
        break;
      case BoundType.Outbound:
        this.searchService.setOutboundDate(dateSelected.selectedDate);
        this.minInboundDate = new Date(dateSelected.selectedDate);
        break;
      default:
        throw new Error('Unknow bound type');
    }
  }
}
