import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DatePickerModule } from '../../elements/date-picker/date-picker.module';
import { DateSearchContComponent } from './containers/date-search-cont/date-search-cont.component';
import { DateSearchPresComponent } from './presenters/date-search-pres/date-search-pres.component';
import { SearchService } from '../../../services/search/search.service';

@NgModule({
  imports: [
    CommonModule,
    DatePickerModule
  ],
  declarations: [DateSearchContComponent, DateSearchPresComponent],
  exports: [DateSearchContComponent, DateSearchPresComponent],
  providers: [SearchService]
})
export class DateSearchModule {
}
