import { inject, TestBed } from '@angular/core/testing';

import { DataProviderService } from './data-provider.service';
import { HttpClientModule } from '@angular/common/http';

describe('DataProviderServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [DataProviderService]
    });
  });

  it('should be created', inject([DataProviderService], (service: DataProviderService) => {
    expect(service).toBeTruthy();
  }));
});
