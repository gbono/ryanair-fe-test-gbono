import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { formatDate } from '../../common/helpers/date.helpers';
import { Availabilities, GlobalInfo, Search } from '../../models/data';

@Injectable()
export class DataProviderService {

  private GLOBAL_INFO_URL = 'https://murmuring-ocean-10826.herokuapp.com/en/api/2/forms/flight-booking-selector/';

  private getAvailabilityUrl = (search: Search) => {
    const formatFn = formatDate('YYYY-MM-DD');
    return `https://murmuring-ocean-10826.herokuapp.com/en/api/2/flights/` +
      `from/${search.outboundAirport}/to/${search.inboundAirport}/` +
      `${formatFn(search.outboundDate)}/${formatFn(search.inboundDate)}/` +
      `250/unique/?limit=15&offset=0`;
  }

  constructor(private http: HttpClient) {
  }

  public getGlobalInfoData(): Observable<GlobalInfo> {
    return this.http.get<GlobalInfo>(this.GLOBAL_INFO_URL);
  }

  public getCheapFlightsData(search: Search): Observable<Availabilities> {
    return this.http.get<Availabilities>(this.getAvailabilityUrl(search));
  }
}
