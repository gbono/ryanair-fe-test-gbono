import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { Availabilities, GlobalInfo, Search } from '../../models/data';
import { of } from 'rxjs/observable/of';

@Injectable()
export class MockDataProviderService {

  private fakeGlobalInfo: GlobalInfo = {
    airports: [],
    countries: [],
    discounts: {},
    messages: {},
    routes: { LGW: ['SNN', 'ORK', 'BFS', 'ALC', 'SVQ', 'KUN', 'DUB'] }
  };

  private fakeAvailResp: Availabilities = {
    flights: [
      {
        dateFrom: '2018-04-29T19:44:22.044Z',
        dateTo: '2018-04-29T20:04:42.766Z',
        currency: '€',
        price: 67.07997207902372
      }
    ]
  };

  private getAvailabilityUrl = (search: Search) => {
    return 'fake.url.com';
  }

  public getGlobalInfoData(): Observable<GlobalInfo> {
    return of(this.fakeGlobalInfo);
  }

  public getCheapFlightsData(search: Search): Observable<Availabilities> {
    return of(this.fakeAvailResp);
  }
}
