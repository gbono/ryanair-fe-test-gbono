import { TestBed } from '@angular/core/testing';

import { GlobalInfoService } from './global-info.service';
import { DataProviderService } from '../data-provider/data-provider.service';
import { MockDataProviderService } from '../data-provider/mock-data-provider.service';
import { SearchService } from '../search/search.service';
import { Subject } from 'rxjs/Subject';
import { LoadGlobalInfo } from '../../actions/global-info/global-info.actions';
import { of } from 'rxjs/observable/of';
import { GlobalInfo } from '../../models';
import { Store } from '@ngrx/store';

describe('GlobalInfoService', () => {
  let globalInfoService: GlobalInfoService;

  const subject = new Subject<any>();
  const mockStore = {
    select: jasmine.createSpy('select').and.returnValue(subject),
    dispatch: jasmine.createSpy('dispatch')
  };

  const fakeGlobalInfo: GlobalInfo = {
    airports: [],
    countries: [],
    discounts: {},
    messages: {},
    routes: { LGW: ['SNN', 'ORK', 'BFS', 'ALC', 'SVQ', 'KUN', 'DUB'] }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        GlobalInfoService,
        { provide: DataProviderService, useClass: MockDataProviderService },
        { provide: Store, useValue: mockStore },
        SearchService
      ]
    });
    globalInfoService = TestBed.get(GlobalInfoService);
  });

  it('should be created', () => {
    expect(globalInfoService).toBeTruthy();
  });

  it('should retrieve global info', async (done) => {
    globalInfoService.loadGlobalInfo();
    expect(mockStore.dispatch).toHaveBeenCalledWith(new LoadGlobalInfo(of(fakeGlobalInfo).toPromise()));
    done();
  });
});
