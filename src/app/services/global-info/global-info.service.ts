import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { combineLatest } from 'rxjs/operators';
import { LoadGlobalInfo } from '../../actions/global-info/global-info.actions';
import { getAirportFromIataCode } from '../../common/helpers/location.helpers';
import { Airport, GlobalInfo } from '../../models';

import * as fromRoot from '../../reducers';
import { getGlobalInfoAirports, getGlobalInfoRoutes, getGloblaInfo } from '../../selectors';
import { DataProviderService } from '../data-provider/data-provider.service';
import { SearchService } from '../search/search.service';
import { isObjEmptyOrUndefined } from '../../common/helpers/miscellaneous.helpers';

@Injectable()
export class GlobalInfoService {

  private readonly _globalInfo$: Observable<GlobalInfo>;
  private readonly _airports$: Observable<Airport[]>;
  private readonly _routes$: Observable<{ [code: string]: string[]; }>;

  constructor(private _store: Store<fromRoot.State>,
              private _dataProviderService: DataProviderService,
              private _searchService: SearchService) {
    this._globalInfo$ = this._store.select(getGloblaInfo);
    this._airports$ = this._store.select(getGlobalInfoAirports);
    this._routes$ = this._store.select(getGlobalInfoRoutes);
  }

  public loadGlobalInfo(): void {
    this._store.dispatch(new LoadGlobalInfo(this._dataProviderService.getGlobalInfoData().toPromise()));
  }

  public get globalInfo$(): Observable<GlobalInfo> {
    return this._globalInfo$;
  }

  public get airports$(): Observable<Airport[]> {
    return this._airports$;
  }

  public get combinableInboundAirports$(): Observable<Airport[]> {
    return this._airports$.pipe(combineLatest(this._routes$, this._searchService.outboundAirport$,
      (airports: Airport[], routes: { [code: string]: string[]; }, outboundAirport: string) => {
        if (isObjEmptyOrUndefined(airports) || isObjEmptyOrUndefined(routes) || !outboundAirport || outboundAirport === '') {
          return [];
        }
        const airportFromIataCode = getAirportFromIataCode(airports);
        return routes[outboundAirport].map(airportFromIataCode);
      }));
  }
}
