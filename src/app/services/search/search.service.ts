import { Injectable } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import {
  NewStateFromUrl, ResetInboundAirport, SelectInboundAirport, SelectInboundDate, SelectOutboundAirport,
  SelectOutboundDate
} from '../../actions/search/search.actions';
import { Airport, Search } from '../../models/data';

import * as fromRoot from '../../reducers';
import {
  getInboundAirport, getInboundDate, getOutboundAirport, getOutboundDate, getSearchFullState
} from '../../selectors';

@Injectable()
export class SearchService {

  private readonly _inboundAirport$: Observable<string>;
  private readonly _outboundAirport$: Observable<string>;
  private readonly _inboundDate$: Observable<Date>;
  private readonly _outboundDate$: Observable<Date>;
  private readonly _searchState$: Observable<Search>;

  public searchForm: NgForm;

  constructor(private _store: Store<fromRoot.State>) {
    this._inboundAirport$ = this._store.select(getInboundAirport);
    this._outboundAirport$ = this._store.select(getOutboundAirport);
    this._inboundDate$ = this._store.select(getInboundDate);
    this._outboundDate$ = this._store.select(getOutboundDate);
    this._searchState$ = this._store.select(getSearchFullState);
  }

  public setInboundAirport(inboundAirport: Airport): void {
    this._store.dispatch(new SelectInboundAirport(inboundAirport));
  }

  public setOutboundAirport(outboundAirport: Airport): void {
    this._store.dispatch(new SelectOutboundAirport(outboundAirport));
  }

  public setInboundDate(inboundDate: Date): void {
    this._store.dispatch(new SelectInboundDate(inboundDate));
  }

  public setOutboundDate(outboundDate: Date): void {
    this._store.dispatch(new SelectOutboundDate(outboundDate));
  }

  public resetInboundLocation(): void {
    this._store.dispatch(new ResetInboundAirport());
  }

  public setNewStateFromUrl(search: Search): void {
    this._store.dispatch(new NewStateFromUrl(search));
  }

  public get inboundAirport$(): Observable<string> {
    return this._inboundAirport$;
  }

  public get outboundAirport$(): Observable<string> {
    return this._outboundAirport$;
  }

  public get inboundDate$(): Observable<Date> {
    return this._inboundDate$;
  }

  public get outboundDate$(): Observable<Date> {
    return this._outboundDate$;
  }

  public get searchState$(): Observable<Search> {
    return this._searchState$;
  }
}

