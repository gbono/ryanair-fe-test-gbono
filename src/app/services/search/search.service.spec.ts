import { TestBed } from '@angular/core/testing';

import { Subject } from 'rxjs/Subject';
import { Store } from '@ngrx/store';
import { SearchService } from './search.service';
import { Search } from '../../models';
import {
  NewStateFromUrl,
  ResetInboundAirport,
  SelectInboundAirport, SelectInboundDate, SelectOutboundAirport, SelectOutboundDate
} from '../../actions/search/search.actions';

describe('Search service', () => {
  let searchService: SearchService;

  const subject = new Subject<any>();
  const mockStore = {
    select: jasmine.createSpy('select').and.returnValue(subject),
    dispatch: jasmine.createSpy('dispatch')
  };

  const fakeSearch: Search = {
    outboundAirport: 'PEG',
    inboundAirport: 'CAG',
    outboundDate: new Date('2018-05-13T00:00:00.000Z'),
    inboundDate: new Date('2018-05-20T00:00:00.000Z')
  };

  const fakeAirport = {
    'iataCode': 'AAR',
    'name': 'Aarhus',
    'base': false,
    'latitude': 56.3,
    'longitude': 10.619,
    'country': {
      'code': 'dk',
      'name': 'Denmark',
      'seoName': 'denmark',
      'englishSeoName': 'denmark',
      'currency': 'DKK',
      'url': 'denmark'
    }
  };

  const fakeDate = new Date();

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        SearchService,
        { provide: Store, useValue: mockStore },
      ]
    });
    searchService = TestBed.get(SearchService);
  });

  it('should be created', () => {
    expect(searchService).toBeTruthy();
  });

  it('should set the inbound airport', async (done) => {
    searchService.setInboundAirport(fakeAirport);
    expect(mockStore.dispatch).toHaveBeenCalledWith(new SelectInboundAirport(fakeAirport));
    done();
  });

  it('should set the outbound airport', async (done) => {
    searchService.setOutboundAirport(fakeAirport);
    expect(mockStore.dispatch).toHaveBeenCalledWith(new SelectOutboundAirport(fakeAirport));
    done();
  });

  it('should set the inbound date', async (done) => {
    searchService.setInboundDate(fakeDate);
    expect(mockStore.dispatch).toHaveBeenCalledWith(new SelectInboundDate(fakeDate));
    done();
  });

  it('should set the outbound date', async (done) => {
    searchService.setOutboundDate(fakeDate);
    expect(mockStore.dispatch).toHaveBeenCalledWith(new SelectOutboundDate(fakeDate));
    done();
  });

  it('should reset the inbound airport', async (done) => {
    searchService.resetInboundLocation();
    expect(mockStore.dispatch).toHaveBeenCalledWith(new ResetInboundAirport());
    done();
  });

  it('should set a new state from Url', async (done) => {
    searchService.setNewStateFromUrl(fakeSearch);
    expect(mockStore.dispatch).toHaveBeenCalledWith(new NewStateFromUrl(fakeSearch));
    done();
  });
});
