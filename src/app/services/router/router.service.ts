import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import * as fromRoot from '../../reducers';
import { getRouterParams } from '../../selectors/router.selectors';
import { RouterState } from '../../models/data/router-state';
import { Go } from '../../actions/router/router.actions';

@Injectable()
export class RouterService {

  private readonly _routerParams$: Observable<{ [key: string]: any }>;

  constructor(private _store: Store<fromRoot.State>) {
    this._routerParams$ = this._store.select(getRouterParams);
  }

  public get routerParams$(): Observable<{ [key: string]: any }> {
    return this._routerParams$;
  }

  public routerNavigate(routeState: Partial<RouterState>): void {
    this._store.dispatch(new Go(routeState));
  }
}
