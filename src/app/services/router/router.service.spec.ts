import { TestBed } from '@angular/core/testing';

import { Subject } from 'rxjs/Subject';
import { Store } from '@ngrx/store';
import { RouterService } from './router.service';
import { Go } from '../../actions/router/router.actions';

describe('Router service', () => {
  let routerService: RouterService;

  const subject = new Subject<any>();
  const mockStore = {
    select: jasmine.createSpy('select').and.returnValue(subject),
    dispatch: jasmine.createSpy('dispatch')
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        RouterService,
        { provide: Store, useValue: mockStore },
      ]
    });
    routerService = TestBed.get(RouterService);
  });

  it('should be created', () => {
    expect(routerService).toBeTruthy();
  });

  it('should dispatch a Go action', async (done) => {
    routerService.routerNavigate({ path: ['mypage'] });
    expect(mockStore.dispatch).toHaveBeenCalledWith(new Go({ path: ['mypage'] }));
    done();
  });
});
