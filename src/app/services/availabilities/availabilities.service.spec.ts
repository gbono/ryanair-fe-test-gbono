import { TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { AvailabilitiesService } from './availabilities.service';
import { Subject } from 'rxjs/Subject';
import { Store } from '@ngrx/store';
import { DataProviderService } from '../data-provider/data-provider.service';
import { Availabilities, Search } from '../../models';
import { LoadAvailabilites } from '../../actions/availabilities/availabilities.actions';
import { MockDataProviderService } from '../data-provider/mock-data-provider.service';
import { of } from 'rxjs/observable/of';

describe('AvailabiltiyService', () => {
  const subject = new Subject<any>();
  const mockStore = {
    select: jasmine.createSpy('select').and.returnValue(subject),
    dispatch: jasmine.createSpy('dispatch')
  };
  let availService: AvailabilitiesService;

  const fakeSearch: Search = {
    outboundAirport: 'PEG',
    inboundAirport: 'CAG',
    outboundDate: new Date('2018-05-13T00:00:00.000Z'),
    inboundDate: new Date('2018-05-20T00:00:00.000Z')
  };

  const fakeAvailResp: Availabilities = {
    flights: [
      {
        dateFrom: '2018-04-29T19:44:22.044Z',
        dateTo: '2018-04-29T20:04:42.766Z',
        currency: '€',
        price: 67.07997207902372
      }
    ]
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [
        AvailabilitiesService,
        { provide: DataProviderService, useClass: MockDataProviderService },
        { provide: Store, useValue: mockStore }
      ]
    });
    availService = TestBed.get(AvailabilitiesService);
  });

  it('should be created', () => {
    expect(availService).toBeTruthy();
  });

  it('should retrieve flights availabilities', async (done) => {
    availService.loadFlights(fakeSearch);
    expect(mockStore.dispatch).toHaveBeenCalledWith(new LoadAvailabilites(of(fakeAvailResp).toPromise()));
    done();
  });
});
