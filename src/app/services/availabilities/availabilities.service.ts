import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Flight, Search } from '../../models';

import * as fromRoot from '../../reducers';
import { DataProviderService } from '../data-provider/data-provider.service';
import { LoadAvailabilites } from '../../actions/availabilities/availabilities.actions';
import { getFlights } from '../../selectors';

@Injectable()
export class AvailabilitiesService {

  private readonly _flightsAvailability$: Observable<Flight[]>;

  constructor(private _store: Store<fromRoot.State>,
              private _dataProviderService: DataProviderService) {
    this._flightsAvailability$ = this._store.select(getFlights);
  }

  public loadFlights(search: Search): void {
    this._store.dispatch(new LoadAvailabilites(this._dataProviderService.getCheapFlightsData(search).toPromise()));
  }

  public get flightsAvailability$(): Observable<Flight[]> {
    return this._flightsAvailability$;
  }
}
