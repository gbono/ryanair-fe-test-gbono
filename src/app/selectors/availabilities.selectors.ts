import { createSelector, createFeatureSelector } from '@ngrx/store';
import { State } from '../reducers/availabilities/availabilities.reducer';

export const getAvailabilitiesState = createFeatureSelector<State>('availabilities');
export const getFlights = createSelector(getAvailabilitiesState, (state: State) => state.content.flights);
