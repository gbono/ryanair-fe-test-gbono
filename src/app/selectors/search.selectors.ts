import { createFeatureSelector, createSelector } from '@ngrx/store';
import { State } from '../reducers/search/search.reducer';

const getSearchState = createFeatureSelector<State>('search');

export const getOutboundAirport = createSelector(getSearchState, (state: State) => state.outboundAirport);
export const getInboundAirport = createSelector(getSearchState, (state: State) => state.inboundAirport);
export const getOutboundDate = createSelector(getSearchState, (state: State) => state.outboundDate);
export const getInboundDate = createSelector(getSearchState, (state: State) => state.inboundDate);
export const getSearchFullState = createSelector(getSearchState, (state: State) => state);
