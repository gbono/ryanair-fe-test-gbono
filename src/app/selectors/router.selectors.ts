import { RouterReducerState } from '@ngrx/router-store';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { RouterStateSnapshot } from '@angular/router';

export const getRouterState = createFeatureSelector<RouterReducerState>('router');
export const getRouterParams = createSelector(getRouterState,
  (state: RouterReducerState<RouterStateSnapshot>) => state.state['params']);
