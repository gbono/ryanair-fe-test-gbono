import { createSelector, createFeatureSelector } from '@ngrx/store';
import { State } from '../reducers/gloabl-info/global-info.reducer';

export const getGlobalInfoState = createFeatureSelector<State>('globalInfo');
export const getGlobalInfoRoutes = createSelector(getGlobalInfoState, (state: State) => state.content.routes);
export const getGlobalInfoAirports = createSelector(getGlobalInfoState, (state: State) => state.content.airports);
export const getGlobalInfoCountries = createSelector(getGlobalInfoState, (state: State) => state.content.countries);
export const getGloblaInfo = createSelector(getGlobalInfoState, (state: State) => state.content);
