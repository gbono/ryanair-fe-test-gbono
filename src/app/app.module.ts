import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { PreloadAllModules, RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';

import { AppComponent } from './app.component';
import { ROUTES } from './app.routes';
import { metaReducers, reducers } from './reducers';
import { DataProviderService } from './services/data-provider/data-provider.service';
import { SearchService } from './services/search/search.service';
import { GlobalInfoEffect } from './effects/global-info/global-info.effect';
import { HttpClientModule } from '@angular/common/http';
import { SearchPageModule } from './components/pages/search-page/search-page.module';
import { AvailabilitiesEffect } from './effects/availabilities/availabilities.effect';
import { RouterStateSerializer, StoreRouterConnectingModule } from '@ngrx/router-store';
import { CustomRouterStateSerializer } from './common/custom-router-serializer';
import { RouterService } from './services/router/router.service';
import { RouterEffects } from './effects/router/router.effects';

const ELEMENTS = [
  AppComponent,
];

const PAGES = [
  SearchPageModule
];

const SERVICES = [
  DataProviderService,
  SearchService,
  RouterService
];

const EFFECTS = [
  GlobalInfoEffect,
  AvailabilitiesEffect,
  RouterEffects
];

@NgModule({
  declarations: [
    ELEMENTS,
  ],
  imports: [
    BrowserModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    RouterModule.forRoot(ROUTES, {
      useHash: Boolean(history.pushState) === false,
      preloadingStrategy: PreloadAllModules
    }),
    StoreRouterConnectingModule.forRoot({
      stateKey: 'router',
    }),
    EffectsModule.forRoot([...EFFECTS]),
    NgbModule.forRoot(),
    HttpClientModule,
    PAGES
  ],
  providers: [
    SERVICES,
    { provide: RouterStateSerializer, useClass: CustomRouterStateSerializer }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
