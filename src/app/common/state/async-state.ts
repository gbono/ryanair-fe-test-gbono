import { StateStatus } from '../action/state-status';

export interface AsyncState {
    stateStatus: StateStatus;
}
