import { Search } from '../../models/data';
import { formatDate } from './date.helpers';
import { allPropsDefined, isObjEmptyOrUndefined } from './miscellaneous.helpers';

export const buildUrlFromSearch: (root: string) => (search: Search) => string = root => search => {
  const formatDateFn = formatDate('YYYY-MM-DD');
  return `/${root}/${search.outboundAirport}/${search.inboundAirport}` +
    `/${formatDateFn(search.outboundDate)}/${formatDateFn(search.inboundDate)}`;
};

export const routerParamsToSearch: (params: any) => Search = params => {
  return !isObjEmptyOrUndefined(params) && allPropsDefined(params) ?
    { ...params, inboundDate: new Date(params.inboundDate), outboundDate: new Date(params.outboundDate) } :
    { inboundDate: null, outboundDate: null, outboundAirport: null, inboundAirport: null };
};
