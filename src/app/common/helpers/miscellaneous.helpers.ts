import { Search } from '../../models/data';

export const allPropsDefined: (obj: { [code: string]: any; }) => boolean = obj => !Object.values(obj).some((prop) => !prop);

export const isObjEmptyOrUndefined: (obj: { [code: string]: any; }) => boolean = obj => !obj || Object.keys(obj).length < 1;

export const searchObjEquals: (search1: Search) => (search2: Search) => boolean = search1 => search2 =>
  !Object.keys(search1).some(key => search1[key] !== search2[key])
