import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { Moment } from 'moment';

export const addMonths: (months: number) => (date: Date) => Date =
  months => date => new Date(date.setMonth(date.getMonth() + months));

export const formatDate: (format: string) => (date: Date) => string = format => date => moment(date).format(format);

export const getDateFromUrl: (urlFormat: string) => (dateFormatted: string) => (Date | null) =
  urlFormat => dateFormatted => {
    const date: Moment = moment(dateFormatted, urlFormat);
    return date.isValid() ? date.toDate() : null;
  };

export const ngbDataStructToDate: (ngbDate: NgbDateStruct) => Date =
  (ngbDate => moment(`${ngbDate.year}-${ngbDate.month}-${ngbDate.day}`, 'YYYY-MM-DD').toDate());

export const dateToNgbDataStruct: (date: Date) => NgbDateStruct | undefined = date => {
  const momentDate = moment(date);
  return date ? { year: momentDate.year(), month: momentDate.month( ) + 1, day: momentDate.date() } : undefined;
};

export const isNgbDataStructAfter = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day > two.day : one.month > two.month : one.year > two.year;

