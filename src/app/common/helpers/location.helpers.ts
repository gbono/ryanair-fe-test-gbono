import { Airport } from '../../models/data';


export const getAirportFromAirportName: (airportName: string, airports: Airport[]) => Airport =
  (airportName, airports) => (airports || []).find((airport: Airport) => airport.name === airportName);

export const getAirportFromIataCode = (airports: Airport[]) => (iataCode: string) =>
  airports.find((airport: Airport) => airport.iataCode === iataCode);

export const getAirportName: (airport: Airport) => string = (airport: Airport) => airport.name;

export const airportNameContains = (searchString: string) => (airport: Airport) =>
  airport.name.toLowerCase().indexOf(searchString.toLowerCase()) > -1;
