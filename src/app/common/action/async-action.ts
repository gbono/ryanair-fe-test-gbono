import { BaseAction } from './base-action';

export interface AsyncAction<T> extends BaseAction {
    asyncPayload: Promise<T>;
}
