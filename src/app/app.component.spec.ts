import { async, TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { RouterTestingModule } from '@angular/router/testing';
import { GlobalInfoService } from './services/global-info/global-info.service';
import { Subject } from 'rxjs/Subject';
import { Store } from '@ngrx/store';
import { MockDataProviderService } from './services/data-provider/mock-data-provider.service';
import { DataProviderService } from './services/data-provider/data-provider.service';
import { SearchService } from './services/search/search.service';

describe('AppComponent', () => {
  const subject = new Subject<any>();
  const mockStore = {
    select: jasmine.createSpy('select').and.returnValue(subject),
    dispatch: jasmine.createSpy('dispatch')
  };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([])
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        GlobalInfoService,
        SearchService,
        { provide: DataProviderService, useClass: MockDataProviderService },
        { provide: Store, useValue: mockStore }
      ]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
