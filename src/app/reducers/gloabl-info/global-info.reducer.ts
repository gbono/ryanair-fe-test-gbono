import {
  GlobalInfoActions,
  GlobalInfoActionTypes,
  UpdateGlobalInfo
} from '../../actions/global-info/global-info.actions';
import { StateStatus } from '../../common/action/state-status';
import { AsyncState } from '../../common/state/async-state';
import { GlobalInfo } from '../../models/data';

export interface State extends AsyncState {
  content: GlobalInfo;
}

const initalStatus: State = {
  content: {
    airports: [],
    countries: [],
    discounts: {},
    messages: {},
    routes: {}
  },
  stateStatus: StateStatus.ready
};

export function reducer(state: State = initalStatus, action: GlobalInfoActions) {

  switch (action.type) {
    case GlobalInfoActionTypes.Load: {
      return {
        ...state,
        stateStatus: StateStatus.loading
      };
    }

    case GlobalInfoActionTypes.Update: {
      return {
        ...state,
        content: (action as UpdateGlobalInfo).payload,
        stateStatus: StateStatus.ready
      };
    }

    case GlobalInfoActionTypes.Fail: {
      return {
        ...state,
        stateStatus: StateStatus.failed
      };
    }

    default: {
      return state;
    }
  }
}
