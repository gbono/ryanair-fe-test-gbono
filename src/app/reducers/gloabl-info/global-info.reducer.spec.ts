import { getTestBed, TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { StateStatus } from '../../common/action/state-status';
import { GlobalInfo } from '../../models';
import { reducer, State } from './global-info.reducer';
import { FailGlobalInfo, LoadGlobalInfo, UpdateGlobalInfo } from '../../actions/global-info/global-info.actions';

describe('GlobalInfo', () => {
  beforeAll(() => getTestBed().platform || TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()));
  describe('reducers', () => {

    const initalStatus: State = {
      content: {
        airports: [],
        countries: [],
        discounts: {},
        messages: {},
        routes: {}
      },
      stateStatus: StateStatus.ready
    };

    const fakeGlobalInfo: GlobalInfo = {
      airports: [],
      countries: [],
      discounts: {},
      messages: {},
      routes: { LGW: ['SNN', 'ORK', 'BFS', 'ALC', 'SVQ', 'KUN', 'DUB'] }
    };
    const fakeError = {
      message: 'oops..i did it again'
    };

    it('should return the initial status', () => {
      const state = reducer(undefined, { type: 'foo' } as any);
      expect(state).toEqual(initalStatus);
      expect(state.stateStatus).toBe(StateStatus.ready);
    });

    describe('on LOAD action', () => {
      it('should change the status', async (done) => {
        const state = reducer(initalStatus, new LoadGlobalInfo(Promise.resolve<GlobalInfo>(fakeGlobalInfo)));
        expect(state.stateStatus).toEqual(StateStatus.loading);
        done();
      });
    });

    describe('on UPDATE action', () => {
      it('should change the status and set a state content', () => {
        const state = reducer(initalStatus, new UpdateGlobalInfo(fakeGlobalInfo));
        expect(state.stateStatus).toEqual(StateStatus.ready);
        expect(state.content).toBe(fakeGlobalInfo);
      });
    });

    describe('on FAIL action', () => {
      it('should change the status', async (done) => {
        const state = reducer(initalStatus, new FailGlobalInfo(fakeError));
        expect(state.stateStatus).toEqual(StateStatus.failed);
        done();
      });
    });
  });
});

