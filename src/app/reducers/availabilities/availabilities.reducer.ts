import { StateStatus } from '../../common/action/state-status';
import { AsyncState } from '../../common/state/async-state';
import { Availabilities } from '../../models/data';
import {
  AvailabilitiesActions, AvailabilitiesActionTypes, UpdateAvailabilities
} from '../../actions/availabilities/availabilities.actions';

export interface State extends AsyncState {
  content: Availabilities;
}

const initalStatus: State = {
  content: {
    flights: []
  },
  stateStatus: StateStatus.ready
};

export function reducer(state: State = initalStatus, action: AvailabilitiesActions) {

  switch (action.type) {
    case AvailabilitiesActionTypes.Load: {
      return {
        ...state,
        stateStatus: StateStatus.loading
      };
    }

    case AvailabilitiesActionTypes.Update: {
      return {
        ...state,
        content: (action as UpdateAvailabilities).payload,
        stateStatus: StateStatus.ready
      };
    }

    case AvailabilitiesActionTypes.Fail: {
      return {
        ...state,
        stateStatus: StateStatus.failed
      };
    }

    default: {
      return state;
    }
  }
}
