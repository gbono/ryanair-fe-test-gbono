import { getTestBed, TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { reducer, State } from './availabilities.reducer';
import { StateStatus } from '../../common/action/state-status';
import {
  FailAvailabilities, LoadAvailabilites, UpdateAvailabilities
} from '../../actions/availabilities/availabilities.actions';
import { Availabilities } from '../../models';

describe('Availabilties', () => {
  beforeAll(() => getTestBed().platform || TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()));
  describe('reducers', () => {

    const initalStatus: State = {
      content: {
        flights: []
      },
      stateStatus: StateStatus.ready
    };
    const fakeAvailResp: Availabilities = {
      flights: [
        {
          dateFrom: '2018-04-29T19:44:22.044Z',
          dateTo: '2018-04-29T20:04:42.766Z',
          currency: '€',
          price: 67.07997207902372
        }
      ]
    };
    const fakeError = {
      message: 'oops..i did it again'
    };

    it('should return the initial status', () => {
      const state = reducer(undefined, { type: 'foo' } as any);
      expect(state).toEqual(initalStatus);
      expect(state.stateStatus).toBe(StateStatus.ready);
    });

    describe('on LOAD action', () => {
      it('should change the status', async (done) => {
        const state = reducer(initalStatus, new LoadAvailabilites(Promise.resolve<Availabilities>(fakeAvailResp)));
        expect(state.stateStatus).toEqual(StateStatus.loading);
        done();
      });
    });

    describe('on UPDATE action', () => {
      it('should change the status and set a state content', () => {
        const state = reducer(initalStatus, new UpdateAvailabilities(fakeAvailResp));
        expect(state.stateStatus).toEqual(StateStatus.ready);
        expect(state.content).toBe(fakeAvailResp);
      });
    });

    describe('on FAIL action', () => {
      it('should change the status', async (done) => {
        const state = reducer(initalStatus, new FailAvailabilities(fakeError));
        expect(state.stateStatus).toEqual(StateStatus.failed);
        done();
      });
    });
  });
});

