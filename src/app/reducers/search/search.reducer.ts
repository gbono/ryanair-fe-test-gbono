import { LocationPickerActions, SearchActionType } from '../../actions/search/search.actions';
import { Search } from '../../models/data';

/* tslint:disable:no-empty-interface*/
export interface State extends Search {
}

/* tslint:enable:no-empty-interface*/

const initialState: State = {
  inboundAirport: null,
  outboundAirport: null,
  inboundDate: null,
  outboundDate: null
};

export function reducer(state: State = initialState, action: LocationPickerActions) {

  switch (action.type) {
    case SearchActionType.SelectOutboundAirport: {
      return {
        ...state,
        outboundAirport: action.payload.iataCode
      };
    }

    case SearchActionType.SelectInboundAirport: {
      return {
        ...state,
        inboundAirport: action.payload.iataCode
      };
    }

    case SearchActionType.SelectInboundDate: {
      return {
        ...state,
        inboundDate: action.payload
      };
    }

    case SearchActionType.SelectOutboundDate: {
      return {
        ...state,
        outboundDate: action.payload
      };
    }

    case SearchActionType.ResetOutboundDate: {
      return {
        ...state,
        outboundDate: null
      };
    }

    case SearchActionType.ResetInboundDate: {
      return {
        ...state,
        inboundDate: null
      };
    }

    case SearchActionType.ResetInboundAirport: {
      return {
        ...state,
        inboundAirport: null
      };
    }

    case SearchActionType.ResetSearchData: {
      return {
        ...initialState
      };
    }

    case SearchActionType.NewStateFromUrl: {
      return {
        ...action.payload,
      };
    }

    default: {
      return state;
    }
  }
}
