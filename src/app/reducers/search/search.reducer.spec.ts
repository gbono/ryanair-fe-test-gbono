import { getTestBed, TestBed } from '@angular/core/testing';
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { reducer, State } from './search.reducer';
import { Search } from '../../models';
import {
  NewStateFromUrl,
  ResetInboundAirport, ResetInboundDate, ResetOutboundDate, ResetSearchData, SelectInboundAirport, SelectInboundDate,
  SelectOutboundAirport, SelectOutboundDate
} from '../../actions/search/search.actions';

describe('Search', () => {
  beforeAll(() => getTestBed().platform || TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting()));
  describe('reducers', () => {

    const initialState: State = {
      inboundAirport: null,
      outboundAirport: null,
      inboundDate: null,
      outboundDate: null
    };

    const fakeSearch: Search = {
      outboundAirport: 'PEG',
      inboundAirport: 'CAG',
      outboundDate: new Date('2018-05-13T00:00:00.000Z'),
      inboundDate: new Date('2018-05-20T00:00:00.000Z')
    };

    const fakeAirport = {
      'iataCode': 'AAR',
      'name': 'Aarhus',
      'base': false,
      'latitude': 56.3,
      'longitude': 10.619,
      'country': {
        'code': 'dk',
        'name': 'Denmark',
        'seoName': 'denmark',
        'englishSeoName': 'denmark',
        'currency': 'DKK',
        'url': 'denmark'
      }
    };

    it('should return the initial status', () => {
      const state = reducer(undefined, { type: 'foo' } as any);
      expect(state).toEqual(initialState);
    });

    describe('on SelectOutboundAirport action', () => {
      it('should change the outbound airport value', async (done) => {
        const state = reducer(initialState, new SelectOutboundAirport(fakeAirport));
        expect(state.outboundAirport).toEqual(fakeAirport.iataCode);
        done();
      });
    });

    describe('on SelectInboundAirport action', () => {
      it('should change the inbound airport value', () => {
        const state = reducer(initialState, new SelectInboundAirport(fakeAirport));
        expect(state.inboundAirport).toEqual(fakeAirport.iataCode);
      });
    });

    describe('on SelectOutboundDate action', () => {
      it('should change the outbound date value', async (done) => {
        const dateNow = new Date();
        const state = reducer(initialState, new SelectOutboundDate(dateNow));
        expect(state.outboundDate).toEqual(dateNow);
        done();
      });
    });

    describe('on SelectInboundDate action', () => {
      it('should change the inbound date value', async (done) => {
        const dateNow = new Date();
        const state = reducer(initialState, new SelectInboundDate(dateNow));
        expect(state.inboundDate).toEqual(dateNow);
        done();
      });
    });

    describe('on ResetOutboundDate action', () => {
      it('should reset the outbound date value', async (done) => {
        const state = reducer(initialState, new ResetOutboundDate());
        expect(state.outboundDate).toEqual(null);
        done();
      });
    });

    describe('on ResetInboundDate action', () => {
      it('should reset the inbound date value', async (done) => {
        const state = reducer(initialState, new ResetInboundDate());
        expect(state.inboundDate).toEqual(null);
        done();
      });
    });

    describe('on ResetInboundAirport action', () => {
      it('should reset the inbound airport value', async (done) => {
        const state = reducer(initialState, new ResetInboundAirport());
        expect(state.inboundAirport).toEqual(null);
        done();
      });
    });

    describe('on ResetSearchData action', () => {
      it('should reset all values', async (done) => {
        const state = reducer(initialState, new ResetSearchData());
        expect(state).toEqual(initialState);
        done();
      });
    });

    describe('on NewStateFromUrl action', () => {
      it('should set all new values', async (done) => {
        const state = reducer(initialState, new NewStateFromUrl(fakeSearch));
        expect(state).toEqual(fakeSearch);
        done();
      });
    });
  });
});

