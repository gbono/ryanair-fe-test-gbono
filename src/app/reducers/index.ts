import * as fromRouter from '@ngrx/router-store';
import { ActionReducer, ActionReducerMap, MetaReducer, } from '@ngrx/store';

import { storeFreeze } from 'ngrx-store-freeze';
import { environment } from '../../environments/environment';
import { RouterStateUrl } from '../common/custom-router-serializer';

import * as fromGlobalInfo from './gloabl-info/global-info.reducer';
import * as fromSearch from './search/search.reducer';
import * as fromAvailabilities from './availabilities/availabilities.reducer';

export interface State {
  router: fromRouter.RouterReducerState<RouterStateUrl>;
  globalInfo: fromGlobalInfo.State;
  search: fromSearch.State;
  availabilities: fromAvailabilities.State;
}

/**
 * Our state is composed of a map of action reducer functions.
 * These reducer functions are called with each dispatched action
 * and the current or initial state and return a new immutable state.
 */
export const reducers: ActionReducerMap<State> = {
  router: fromRouter.routerReducer,
  globalInfo: fromGlobalInfo.reducer,
  search: fromSearch.reducer,
  availabilities: fromAvailabilities.reducer
};

// console.log all actions
export function logger(reducer: ActionReducer<State>): ActionReducer<State> {
  return function (state: State, action: any): State {
    console.log('state', state);
    console.log('action', action);

    return reducer(state, action);
  };
}

/**
 * By default, @ngrx/store uses combineReducers with the reducer map to compose
 * the root meta-reducer. To add more meta-reducers, provide an array of meta-reducers
 * that will be composed to form the root meta-reducer.
 */
export const metaReducers: MetaReducer<State>[] = !environment.production
  ? [logger, storeFreeze]
  : [];
