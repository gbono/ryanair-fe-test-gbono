import { Routes } from '@angular/router';
import { SearchPageComponent } from './components/pages/search-page/search-page.component';

export const ROUTES: Routes = [
  {path: '', component: SearchPageComponent},
  {path: 'search', component: SearchPageComponent},
  {path: '**', component: SearchPageComponent},
];
